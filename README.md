# Image File Preview

Files preview in view field.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/image_file_preview).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/image_file_preview).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

or

Install via composer
- `composer require drupal/image_file_preview`
- `drush en image_file_preview -y`


## Configuration

This module have no Configuration.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat - [radheymkumar](https://www.drupal.org/u/radheymkumar)
